#include <math.h>
#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]
#include <limits>
#include <iostream>

using namespace std;

// [[Rcpp::export]]  
void swapRcpp(Rcpp::IntegerVector v, int i, int j) 
{
    int t = v(i-1);
    v(i-1) = v(j-1);
    v(j-1) = t;
}

// [[Rcpp::export]]
RcppExport SEXP logliks(Rcpp::NumericMatrix Y, Rcpp::NumericMatrix D, double theta) 
{
  int m = D.nrow();
  int n = Y.nrow();
  
  if (Y.ncol() != m) 
  {
    cout << "dimension mismatch; ncol(Y) != nrow(D)";
    return(0);
  }
  
  Rcpp::NumericMatrix Sigma(D.nrow(), D.ncol());
  // sqrt(eps_mach)
  double eps = pow(10,-8);
  for(int i = 0; i < D.nrow(); i++)
  {
    for(int j = 0; j < D.ncol(); j++)
    {
      Sigma(i, j) = pow(arma::datum::e, -1.0*D(i, j) / theta);
      if (i == j)
      {
        Sigma(i, i) += eps;
      }
    }
  }
  
  arma::mat cholSigma = arma::chol(Rcpp::as<arma::mat> (Sigma) );
  double ldet = 2 * arma::sum(log(cholSigma.diag()));
  double ll = -.5 * n * (m * log(2 * arma::datum::pi) + ldet);
  
  arma::mat cholSigmaInv = arma::inv(trimatu(cholSigma));
  arma::rowvec v;
  for (int i = 0; i < n; i++) 
  {
    v = Rcpp::as<arma::mat> (Y).row(i) * cholSigmaInv;
    ll -= 0.5 * arma::dot(v, v);    
  }  
  
  return(Rcpp::wrap(ll));
}