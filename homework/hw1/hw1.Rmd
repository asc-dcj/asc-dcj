---
title: "Homework 1"
subtitle: "Advanced Statistical Computing (STAT 6984)"
author: "Dan Junker (<dcj2114@vt.edu>) <br> Virginia Tech Department of Mathematics"
output: html_document
---

```{r, echo=FALSE}
options(width=80)
library(knitr)
knit_hooks$set(no.main = function(before, options, envir) {
    if (before) par(mar = c(4.1, 4.1, 1.1, 1.1))  # smaller margin on top
})
```

```{r}
counter <<- 0
```

### Problem 3: Optimization (34 pts)

Write an R function returning
$$
f(x) = 1 - x + 3x^2 - x^3, \quad x \in [-0.5, 2.5].
$$

a. *Plot the function over that range and note the critical points. Check them against the truth (via calculus).*

First we define the function.

```{r}
f <- function(x)
{
  counter <<- counter + 1
  1 - x + 3*x^2 - x^3
}
```

Then plot it over the given range, depicted in the figure below.

```{r, dev.args = list(bg = 'transparent'), fig.width=5, fig.height=4, fig.align="center", no.main=TRUE}
xx <- seq(-0.5, 2.5, length=1000)
plot(xx, f(xx), type="l", xlab="x", ylab="f(x)")
```

b. *Use an R library function to find those critical points numerically, and check them against the plot/truth.*

To find the points numerically we can use the 'optimize' function.

```{r}
counter <<- 0
out <- optimize(f, lower=-0.5, upper=1)
min1 <- out$minimum
minits = counter
counter <<- 0
out <- optimize(f, lower=1, upper = 2.5,maximum = TRUE)
max1 <-out$maximum
maxits = counter
```

The minimum is `r min1` and the maximum is `r max1`.

To check these numbers we note the derivative of the given function is $f'(x)=-1+6x-3x^2$, which has roots $x = \frac{1}{3}(3\pm\sqrt{6})$. Numerically, these roots are about $0.184$ and $1.817$, which match our outputs as expected.

c. *How many iterations (as counted by the number of evaluations of your R function) did it take to find each critical point?*

For the minimum point we required `r minits` function calls and for the maximum we required `r maxits` function calls.

