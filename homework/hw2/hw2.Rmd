---
title: "Homework 2"
author: "Dan Junker (<dcj2114@vt.edu>) <br> Virginia Tech Department of Mathematics"
subtitle: Advanced Statistical Computing (STAT 6984)
output: html_document
---

```{r, echo=FALSE}
options(width=80)
library(knitr)
knit_hooks$set(no.main = function(before, options, envir) {
    if (before) par(mar = c(4.1, 4.1, 1.1, 1.1))  # smaller margin on top
})
```

### Problem 2: Immutable objects (30 pts)

a.  *Report on how much time it takes to swap two elements (`i=1; j=2`) directly at the R command prompt, i.e., without wrapping in a function.*

First we load in given swap function, which we'll use soon.

```{r 1}
swap <- function(v, i, j) 
  { 
	tmp <- v[i]
	v[i] <- v[j]
	v[j] <- tmp
  }
``` 

Then we execute swap code from the command line and report the time.

```{r 2}
v <- 1:1000000000
system.time({tmp <- v[1]
v[1] <- v[2]
v[2] <- tmp})
```

b.  *Write a new version of the `swap` function, called `swap.eval`, which uses `quote` and `eval` to perform the calculation just like in part a. but within the function environment and without copying `v` by working on `v` in the `parent.frame`.  Although this is a toy example, a similar code might be useful if, say, indicies `i` and `j` required substantial pre-calculation within the function before the swap occurred.  Demonstrate your `swap.eval` with `i=1; j=2` and report on the time.*

```{r }
swap.eval <- function(v,i,j) 
{ 
	a <- quote({tmp <- v[i]
	v[i] <- v[j]
	v[j] <- tmp})
  eval(a, envir = parent.frame())
}
v <- 1:1000000000
i <- 1
j <- 2
system.time(swap.eval(v,i,j))
system.time(swap.eval(v,i,j))
```

While this first run takes roughly the same time as the naive implementation, the subsequent run timed in at 0.039 seconds, and following runs timed in at zero, i.e. less than one thousandth of a second.

c. *Write a similar function named `swap.do` which can be called via `do.call` that similarly accesses `v` in the parent frame.  Add a `print` statement at the end of `swap.do` to show the first five elements of `v` after the swap occurs.  Demonstrate `swap.do` with `i=1; j=2` and report on the time.  Are there any disadvantages to `swap.do` compared to `swap.eval`?*
`
```{r}
swap.do <- function(v,i,j)
{
  do.call("swap", list(v,i,j), quote=TRUE, envir=parent.frame())
	print(v[1:5])
}
v <- 1:1000000000
i <- 1
j <- 2
system.time(swap.do(v,i,j))
system.time(swap.do(v,i,j))
```

It appears that using do.call is more compact but slower and does not increase in speed after one or more repetitions.

### Problem 3: Bisection broadening (30 pts)
 
Recall the bisection algorithm and `S3` object-oriented implementation from class.  The bisection method can be generalized to deal with the case $f(x_l) f(x_r) > 0$, by *broadening* the bracket.  That is,  

- reduce $x_l$ and/or increase $x_r$, and try again.
- A reasonable choice is to double the width of the interval, i.e., 

$$
\begin{aligned}
m &\leftarrow (x_l + x_r)/2, & w &\leftarrow x_r - x_l \\
x_l&\leftarrow m - w, & x_r &\leftarrow m + w.
\end{aligned}
$$

Your tasks are the following.

a.  *Incorporate bracketing into the \R{bisection()} function we coded. Note that broadening is not guaranteed to find $x_l$ and $x_r$ such that $f(x_l) f(x_r) \leq 0$, so you should include a limit on the number of times broadening is successively tried with a sensible default.*

```{r}
## bisection method function; must have xl < xr
## and f(xl)*f(xr) < 0;  output is a root of f
## in the interval (xl, xr)
bisection <- function(f, xl, xr, bracketsteps=5, tol=sqrt(.Machine$double.eps),
                      verb=0)
  {
    ## create an output object
    out <- list(f=f, tol=tol)
    class(out) <- "bisection"
    
    ## check inputs
    if(xl > xr) stop("must have xl < xr")

    ## setup and check outputs
    fl <- f(xl)
    fr <- f(xr)
    out$bracket <- data.frame(xl=xl, xr=xr,fl=fl,fr=fr)
    out$prog <- data.frame(xl=xl, xr=xr, fl=fl, fr=fr)
    out$bracketsteps = 0
    if(fl == 0) { out$ans <- xl; return(out) }
    else if(fr == 0) { out$ans <- xr; return(out) }
    else if(fl * fr > 0)
    {
      if(bracketsteps != 0) { print("f(xl) * f(xr) > 0, attempting to widen search.") }
      bracketsuccess = FALSE
      for(i in 1:bracketsteps)
      {
        m <- .5*(xl + xr)
        w <- xr - xl
        xl <- m - w
        xr <- m + w
        fl <- f(xl)
        fr <- f(xr)
        out$bracket[i,] <- c(xl, xr, fl, fr)
        if(verb > 0)
          cat("i=", n, ", (xl, xr)=(", xl, ", ", xr, ")\n", sep="")
        if(fl == 0) { out$ans <- xl; return(out) }
        if(fr == 0) { out$ans <- xr; return(out) }
        
        if(fl * fr < 0) 
        { 
          bracketsuccess = TRUE
          out$bracketsteps = i
          out$xl = xl
          out$xr = xr
          break
        }
      }
      if(!bracketsuccess) { stop("f(xl) * f(xr) > 0 and bracket step limit reached.") }
    }

    ## successively refine xl and xr
    n <- 1
    while((xr - xl) > tol) {
      xm <- (xl + xr)/2
      fm <- f(xm)
      if(fm == 0) { out$ans <- xm; return(out) }
      else if (fl * fm < 0) {
        xr <- xm; fr <- fm
      } else { xl <- xm; fl <- fm }

      ## next iteration
      n <- n + 1
      out$prog[n,] <- c(xl, xr, fl, fr)
      if(verb > 0)
        cat("n=", n, ", (xl, xr)=(", xl, ", ", xr, ")\n", sep="")
    }

    ## return (approximate) root
    out$ans <- (xl + xr)/2
    return(out)
}
```

b.  *Use your modified function to find a root of the (original) function $f(x)$ we used in class, but with a different starting interval of $x_l = 2$ and $x_r = 3$, i.e., not containing the root we found in class.*

```{r}
f <- function(x) { log(x) - exp(-x) }
fr <- bisection(f,2,3)
fr
```

c.  *Use your modified function find the root of*
$$
h(x) = (x - 1)^3 - 2x^2 - \sin(x),
$$ 

*starting with $x_l = 1$ and $x_r = 2$.*

We can define a class called bisector that incorporates the bisection function and the output it generates

```{r}
bisector <- list()
class(bisector) <- "bisector"
bisect.bisector <- bisection
print.bisector <- function(x, ...) 
{
  cat("Root of:\n")
  print(x$out$f)
  cat("in (", x$out$prog$xl[1], ", ", x$out$prog$xr[1],
      ") found after ", nrow(x$out$prog), " iterations: ",
      x$ans, "\n", "to a tolerance of ", x$out$tol, "\n", sep="")
  if(x$out$bracketsteps != 0)
  {
    cat("Search parameters were widened ",x$out$bracketsteps," times, to a search interval of [", x$out$xl,", ",x$out$xr,"]")
  }
}
```

and then use the classes print to generate a brief summary of the algorithm

```{r}
f <- function(x) { (x-1)^3-2*x^2-sin(x) }
bisector$out <- bisect.bisector(f,1,2)
print.bisector(bisector)
```

*For full credit you must keep everything in the S3 environment with appropriate modifications to your generic methods, etc.  You will be judged on style here, in terms of code, `S3` behavior, and writeup/demonstration.  You may have a separate `bisection.R` file with your `S3` library functions, however your writeup must verbally describe how those functions have changed.  I will check for `bisection.R` in your repository against your description.*

We modified the out structure to have a second data frame recording the attempts to widen the search space. The function now takes an input for the number of bracket widening steps we will allow. These final positions and the number of steps to get there are recorded and displayed in the print function for the bisector class.

### Problem 4: R scripts from the Unix prompt (25 pts)

**R provides two "commands" to execute scripts (e.g., some R code in a file like `script.R`).  What are those commands and what are their differences? Please provide a brief high level description and do not plagiarize.* 

The commands are R CMD BATCH and Rscript.

*Focus on details like:*

*- What happens with plots?*

For plots R CMD BATCH (hereafter CMD) creates a pdf with an image of the plot, an Rout file with the system timers and version of R used to execute the code, while Rscript creates only the pdf of the plot.

*- What happens with text output that would normally be printed to the screen?  What about warnings or errors?*

CMD will not print any text to the screen. Rscript will print text to the screen. Similarly, CMD will not print warnings or errors (to the screen) but Rscript will. 

*- What happens with the objects in the workspace when the script terminates.*

CMD creates an RData file with the objects while Rscript deletes them.

*And be sure to contrast default behavior with options for customization.*

Rscript has options to create the files similar to CMD, if desired.

*Write an R script which renders an `Rmarkdown` document in HTML and furnishes a companion `.R` file containing an extraction of the raw code.  The script must entirely comprise of `R` commands (no manual steps like clicking buttons in RStudio).  If you are composing this solution in `Rmarkdown` you may use your homework file `hw2_sol.Rmd` as an example file, generating `hw2_sol.html` and `hw2_sol.R`.  Otherwise, you will need to create a simple dummy one for the purposes of illustration.* 

This script is in this directory, called "hw2script.R".

*Then describe the single Unix command that you would need to call from the command prompt to cause your `.html` and `.R` file to be generated from the `Rmd` file.*

Rscript hw2script.R

*Create a shell script, which is a text file, called `build.sh` with the following two lines*

```
#!/bin/bash
insert your unix command for building from hw2_sol.Rmd here
```

*Make the script file executable with the command `chmod +x build.sh`, and now you can run it from the command line with `./build.sh`.  Make sure it all works, and that it has been added to your repository in `hw2/` because it will be tested.*

*(Note, don't add the output `.html` and `.R` files to your repository.  Don't forget to add the new commands you've learned to your `unix.txt` file.)*
